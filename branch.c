#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "math.h"

int main(int argc, char** argv)
{

//####################### DECLARACION DE VARIABLES GLOBALES #######################
  int s;
  int bp;
  int gh;
  int ph;
  char c;
  u_int32_t estado_inicial;
  u_int32_t direccion = 0;
  char resultado;
  char prediccion;
  char prediccion_g;
  char prediccion_p;
  int number_of_branches = 0;
  int correct_taken = 0;
  int incorrect_taken = 0;
  int correct_notTaken = 0;
  int incorrect_notTaken = 0;
  float correct_percentage = 0;

//####################### LECTURA DE ARGUMENTOS #######################
  for (int i = 1; i <= 8; i++)
  {
    if (strcmp(argv[i], "-s") == 0)
    {
      s = atoi(argv[i+1]);
    } else if (strcmp(argv[i], "-bp") == 0) {
      bp = atoi(argv[i+1]);
    } else if (strcmp(argv[i], "-gh") == 0) {
      gh = atoi(argv[i+1]);
    } else if (strcmp(argv[i], "-ph") == 0) {
      ph = atoi(argv[i+1]);
    }
  }

  if (bp == 0)
  {
//####################### BIMODAL #######################

//declaracion de variables locales
  estado_inicial = 0;
  u_int32_t clear1 = -1;
  clear1 = clear1 >> 32 - s;
  int tamano_tabla_contadores = (int)pow(2,s);
  u_int32_t prediccion_t[tamano_tabla_contadores];
  int caso = 0;
  u_int32_t indice = 0;

//aplicar valores iniciales
  for (int i = 0; i < tamano_tabla_contadores; i++)
  {
    prediccion_t[i] = estado_inicial;
  }

//procesamiento de datos
  c = getchar();
  while(c != EOF)
  {
    if (c == ' ')
    {
      caso = 1;
    } else if (c == '\n') {
      indice = direccion & clear1;
      if (prediccion_t[indice] <= 1)
      {
        prediccion = 'N';
      } else {
        prediccion = 'T';
      }
      if (resultado == 'T')
      {
        if (prediccion_t[indice] != 3)
        {
          prediccion_t[indice]++;
        }
      } else if (resultado == 'N') {
        if (prediccion_t[indice] != 0)
        {
          prediccion_t[indice]--;
        }
      }
      if (resultado != prediccion)
      {
        if (resultado == 'T')
        {
          incorrect_taken++;
        } else if (resultado == 'N') {
          incorrect_notTaken++;
        }
      } else {
        if (resultado == 'T')
        {
          correct_taken++;
        } else if (resultado == 'N') {
          correct_notTaken++;
        }
      }
      caso = 0;
      direccion = 0;
      number_of_branches++;
    } else {
      if (caso == 0)
      {
        direccion = direccion*10;
        direccion += (int)c - 48;
      } else if (caso == 1){
        resultado = c;
      }
    }
    c = getchar();
  }
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Prediction parameters:\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Branch prediction type: Bimodal\n");
  printf("\n");
  printf("BHT size (entries): %i\n", tamano_tabla_contadores);
  printf("\n");
  printf("Global history register size: %i\n", gh);
  printf("\n");
  printf("Private history register size: %i\n", ph);
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Simulation results:\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Number of branches: %i\n", number_of_branches);
  printf("\n");
  printf("Number of correct predictions of taken branches: %i\n", correct_taken);
  printf("\n");
  printf("Number of incorrect predictions of taken branches: %i\n", incorrect_taken);
  printf("\n");
  printf("Number of correct predictions of not-taken branches: %i\n", correct_notTaken);
  printf("\n");
  printf("Number of incorrect predictions of not-taken branches: %i\n", incorrect_notTaken);
  printf("\n");
  printf("Percentage of correct predictions: %.2f\n", (float)((float)((float)(correct_taken+correct_notTaken)/(float)number_of_branches))*100);
  printf("------------------------------------------------------------------------\n");

  } else if (bp == 1) {
//####################### HISTORIA PRIVADA #######################

//declaracion de variables locales
  estado_inicial = 0;
  u_int32_t clear0 = -1;
  u_int32_t clear1 = -1;
  clear1 = clear1 >> 32 - s;
  clear0 = clear0 >> 32 - ph;
  int tamano_tabla_contadores = (int)pow(2,s);
  int tamano_tabla_historia = tamano_tabla_contadores;
  u_int32_t historia_t[tamano_tabla_historia];
  u_int32_t prediccion_t[tamano_tabla_contadores];
  int caso = 0;
  u_int32_t indice_historia = 0;
  u_int32_t indice_prediccion = 0;

//aplicar valores iniciales
  for (int i = 0; i < tamano_tabla_contadores; i++)
  {
    prediccion_t[i] = estado_inicial;
    historia_t[i] = estado_inicial;
  }

//procesamiento de datos
  c = getchar();
  int contero = 1;
  while(c != EOF)
  {
    if (c == ' ')
    {
      caso = 1;
    } else if (c == '\n') {
      indice_historia = direccion & clear1;
      indice_prediccion = indice_historia ^ historia_t[indice_historia];
      if (prediccion_t[indice_prediccion] <= 1)
      {
        prediccion = 'N';
      } else {
        prediccion = 'T';
      }
      if (resultado == 'T')
      {
        historia_t[indice_historia] = (historia_t[indice_historia] << 1) + 1;
        historia_t[indice_historia] = historia_t[indice_historia] & clear0;
        if (prediccion_t[indice_prediccion] != 3)
        {
          prediccion_t[indice_prediccion]++;
        }
      } else if (resultado == 'N') {
        historia_t[indice_historia] = historia_t[indice_historia] << 1;
        historia_t[indice_historia] = historia_t[indice_historia] & clear0;
        if (prediccion_t[indice_prediccion] != 0)
        {
          prediccion_t[indice_prediccion]--;
        }
      }
      if (resultado != prediccion)
      {
        if (resultado == 'T')
        {
          incorrect_taken++;
        } else if (resultado == 'N') {
          incorrect_notTaken++;
        }
      } else {
        if (resultado == 'T')
        {
          correct_taken++;
        } else if (resultado == 'N') {
          correct_notTaken++;
        }
      }
      caso = 0;
      direccion = 0;
      number_of_branches++;
    } else {
      if (caso == 0)
      {
        direccion = direccion*10;
        direccion += (int)c - 48;
      } else if (caso == 1){
        resultado = c;
      }
    }
    c = getchar();
  }

  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Prediction parameters:\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Branch prediction type: PShare\n");
  printf("\n");
  printf("BHT size (entries): %i\n", tamano_tabla_contadores);
  printf("\n");
  printf("Global history register size: %i\n", gh);
  printf("\n");
  printf("Private history register size: %i\n", ph);
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Simulation results:\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Number of branches: %i\n", number_of_branches);
  printf("\n");
  printf("Number of correct predictions of taken branches: %i\n", correct_taken);
  printf("\n");
  printf("Number of incorrect predictions of taken branches: %i\n", incorrect_taken);
  printf("\n");
  printf("Number of correct predictions of not-taken branches: %i\n", correct_notTaken);
  printf("\n");
  printf("Number of incorrect predictions of not-taken branches: %i\n", incorrect_notTaken);
  printf("\n");
  printf("Percentage of correct predictions: %.2f\n", (float)((float)((float)(correct_taken+correct_notTaken)/(float)number_of_branches))*100);
  printf("\n");
  printf("------------------------------------------------------------------------\n");

  } else if (bp == 2) {
//####################### HISTORIA GLOBAL #######################

//declaracion de variables locales
  estado_inicial = 0;
  u_int32_t clear0 = -1;
  u_int32_t clear1 = -1;
  clear1 = clear1 >> 32 - s;
  clear0 = clear0 >> 32 - gh;
  int tamano_tabla_contadores = (int)pow(2,s);
  int tamano_tabla_historia = 1;
  u_int32_t historia_t[tamano_tabla_historia];
  u_int32_t prediccion_t[tamano_tabla_contadores];
  int caso = 0;
  u_int32_t indice_historia = 0;
  u_int32_t indice_prediccion = 0;

//aplicar valores iniciales
  for (int i = 0; i < tamano_tabla_contadores; i++)
  {
    prediccion_t[i] = estado_inicial;
  }
  historia_t[0] = estado_inicial;

//procesamiento de datos
  c = getchar();
  int contero = 1;
  while(c != EOF)
  {
    if (c == ' ')
    {
      caso = 1;
    } else if (c == '\n') {
      indice_historia = direccion & clear1;
      indice_prediccion = indice_historia ^ historia_t[0];
      if (prediccion_t[indice_prediccion] <= 1)
      {
        prediccion = 'N';
      } else {
        prediccion = 'T';
      }
      if (resultado == 'T')
      {
        historia_t[0] = (historia_t[0] << 1) + 1;
        historia_t[0] = historia_t[0] & clear0;
        if (prediccion_t[indice_prediccion] != 3)
        {
          prediccion_t[indice_prediccion]++;
        }
      } else if (resultado == 'N') {
        historia_t[0] = historia_t[0] << 1;
        historia_t[0] = historia_t[0] & clear0;
        if (prediccion_t[indice_prediccion] != 0)
        {
          prediccion_t[indice_prediccion]--;
        }
      }
      if (resultado != prediccion)
      {
        if (resultado == 'T')
        {
          incorrect_taken++;
        } else if (resultado == 'N') {
          incorrect_notTaken++;
        }
      } else {
        if (resultado == 'T')
        {
          correct_taken++;
        } else if (resultado == 'N') {
          correct_notTaken++;
        }
      }
      caso = 0;
      direccion = 0;
      number_of_branches++;
    } else {
      if (caso == 0)
      {
        direccion = direccion*10;
        direccion += (int)c - 48;
      } else if (caso == 1){
        resultado = c;
      }
    }
    c = getchar();
  }

  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Prediction parameters:\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Branch prediction type: GShare\n");
  printf("\n");
  printf("BHT size (entries): %i\n", tamano_tabla_contadores);
  printf("\n");
  printf("Global history register size: %i\n", gh);
  printf("\n");
  printf("Private history register size: %i\n", ph);
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Simulation results:\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Number of branches: %i\n", number_of_branches);
  printf("\n");
  printf("Number of correct predictions of taken branches: %i\n", correct_taken);
  printf("\n");
  printf("Number of incorrect predictions of taken branches: %i\n", incorrect_taken);
  printf("\n");
  printf("Number of correct predictions of not-taken branches: %i\n", correct_notTaken);
  printf("\n");
  printf("Number of incorrect predictions of not-taken branches: %i\n", incorrect_notTaken);
  printf("\n");
  printf("Percentage of correct predictions: %.2f\n", (float)((float)((float)(correct_taken+correct_notTaken)/(float)number_of_branches))*100);
  printf("\n");
  printf("------------------------------------------------------------------------\n");

  } else if (bp == 3) {
//####################### TORNEO #######################

//declaracion de variables locales
  estado_inicial = 0;
  u_int32_t clear0_g = -1;
  u_int32_t clear1 = -1;
  clear1 = clear1 >> 32 - s;
  clear0_g = clear0_g >> 32 - gh;
  int tamano_tabla_contadores = (int)pow(2,s);
  int tamano_tabla_historia_g = 1;
  u_int32_t historia_t_g[tamano_tabla_historia_g];
  u_int32_t prediccion_t_g[tamano_tabla_contadores];
  int caso = 0;
  u_int32_t indice_historia_g = 0;
  u_int32_t indice_prediccion_g = 0;
  u_int32_t clear0_p = -1;
  clear0_p = clear0_p >> 32 - ph;
  int tamano_tabla_historia_p = tamano_tabla_contadores;
  u_int32_t historia_t_p[tamano_tabla_historia_p];
  u_int32_t prediccion_t_p[tamano_tabla_contadores];
  u_int32_t indice_historia_p = 0;
  u_int32_t indice_prediccion_p = 0;
  u_int32_t indice_meta_prediccion = 0;
  u_int32_t meta_prediccion_t[tamano_tabla_contadores];

//aplicar valores iniciales
  for (int i = 0; i < tamano_tabla_contadores; i++)
  {
    prediccion_t_g[i] = estado_inicial;
    prediccion_t_p[i] = estado_inicial;
    historia_t_p[i] = estado_inicial;
    meta_prediccion_t[i] = estado_inicial;
  }
  historia_t_g[0] = estado_inicial;

//procesamiento de datos
  c = getchar();
  int contero = 1;
  while(c != EOF)
  {
    if (c == ' ')
    {
      caso = 1;
    } else if (c == '\n') {
      indice_historia_g = direccion & clear1;
      indice_prediccion_g = indice_historia_g ^ historia_t_g[0];
      indice_historia_p = direccion & clear1;
      indice_prediccion_p = indice_historia_p ^ historia_t_p[indice_historia_p];
      indice_meta_prediccion = direccion & clear1;
      if (prediccion_t_g[indice_prediccion_g] <= 1)
      {
        prediccion_g = 'N';
      } else {
        prediccion_g = 'T';
      }
      if (prediccion_t_p[indice_prediccion_p] <= 1)
      {
        prediccion_p = 'N';
      } else {
        prediccion_p = 'T';
      }
      if (meta_prediccion_t[indice_meta_prediccion] <= 1)
      {
        prediccion = prediccion_p;
      } else {
        prediccion = prediccion_g;
      }
      if ((prediccion_p == resultado) && (prediccion_g != resultado))
      {
        if (meta_prediccion_t[indice_meta_prediccion] != 0)
        {
          meta_prediccion_t[indice_meta_prediccion]--;
        }
      } else if ((prediccion_g == resultado) && (prediccion_p != resultado)) {
        if (meta_prediccion_t[indice_meta_prediccion] != 3)
        {
          meta_prediccion_t[indice_meta_prediccion]++;
        }
      }
      if (resultado == 'T')
      {
        historia_t_g[0] = (historia_t_g[0] << 1) + 1;
        historia_t_g[0] = historia_t_g[0] & clear0_g;
        if (prediccion_t_g[indice_prediccion_g] != 3)
        {
          prediccion_t_g[indice_prediccion_g]++;
        }
      } else if (resultado == 'N') {
        historia_t_g[0] = historia_t_g[0] << 1;
        historia_t_g[0] = historia_t_g[0] & clear0_g;
        if (prediccion_t_g[indice_prediccion_g] != 0)
        {
          prediccion_t_g[indice_prediccion_g]--;
        }
      }
      if (resultado == 'T')
      {
        historia_t_p[indice_historia_p] = (historia_t_p[indice_historia_p] << 1) + 1;
        historia_t_p[indice_historia_p] = historia_t_p[indice_historia_p] & clear0_p;
        if (prediccion_t_p[indice_prediccion_p] != 3)
        {
          prediccion_t_p[indice_prediccion_p]++;
        }
      } else if (resultado == 'N') {
        historia_t_p[indice_historia_p] = historia_t_p[indice_historia_p] << 1;
        historia_t_p[indice_historia_p] = historia_t_p[indice_historia_p] & clear0_p;
        if (prediccion_t_p[indice_prediccion_p] != 0)
        {
          prediccion_t_p[indice_prediccion_p]--;
        }
      }
      if (resultado != prediccion)
      {
        if (resultado == 'T')
        {
          incorrect_taken++;
        } else if (resultado == 'N') {
          incorrect_notTaken++;
        }
      } else {
        if (resultado == 'T')
        {
          correct_taken++;
        } else if (resultado == 'N') {
          correct_notTaken++;
        }
      }
      caso = 0;
      direccion = 0;
      number_of_branches++;
    } else {
      if (caso == 0)
      {
        direccion = direccion*10;
        direccion += (int)c - 48;
      } else if (caso == 1){
        resultado = c;
      }
    }
    c = getchar();
  }

  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Prediction parameters:\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Branch prediction type: Tournament\n");
  printf("\n");
  printf("BHT size (entries): %i\n", tamano_tabla_contadores);
  printf("\n");
  printf("Global history register size: %i\n", gh);
  printf("\n");
  printf("Private history register size: %i\n", ph);
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Simulation results:\n");
  printf("\n");
  printf("------------------------------------------------------------------------\n");
  printf("\n");
  printf("Number of branches: %i\n", number_of_branches);
  printf("\n");
  printf("Number of correct predictions of taken branches: %i\n", correct_taken);
  printf("\n");
  printf("Number of incorrect predictions of taken branches: %i\n", incorrect_taken);
  printf("\n");
  printf("Number of correct predictions of not-taken branches: %i\n", correct_notTaken);
  printf("\n");
  printf("Number of incorrect predictions of not-taken branches: %i\n", incorrect_notTaken);
  printf("\n");
  printf("Percentage of correct predictions: %.2f\n", (float)((float)((float)(correct_taken+correct_notTaken)/(float)number_of_branches))*100);
  printf("\n");  
  printf("------------------------------------------------------------------------\n");

  } else {
//####################### ERROR #######################
    printf("Argumentos incorrectos");
  }
  return 0;
}
